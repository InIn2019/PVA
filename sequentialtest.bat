del res.csv
del result.txt

java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager " "
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Brassard
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-asc.csv" Brassard Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-asc.csv" Brassard Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-asc.csv" Brassard Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-asc.csv" Brassard Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-asc.csv" Brassard Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-asc.csv" Brassard Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-des.csv" Brassard Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-des.csv" Brassard Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-des.csv" Brassard Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-des.csv" Brassard Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-des.csv" Brassard Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-des.csv" Brassard Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-rand.csv" Brassard Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-rand.csv" Brassard Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-rand.csv" Brassard Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-rand.csv" Brassard Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-rand.csv" Brassard Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-rand.csv" Brassard Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager " "
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Sampling
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-asc.csv" Sampling Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-asc.csv" Sampling Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-asc.csv" Sampling Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-asc.csv" Sampling Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-asc.csv" Sampling Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-asc.csv" Sampling Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-des.csv" Sampling Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-des.csv" Sampling Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-des.csv" Sampling Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-des.csv" Sampling Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-des.csv" Sampling Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-des.csv" Sampling Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-rand.csv" Sampling Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-rand.csv" Sampling Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-rand.csv" Sampling Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-rand.csv" Sampling Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-rand.csv" Sampling Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-rand.csv" Sampling Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager " "
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager TDigest
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-asc.csv" TDigest Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-asc.csv" TDigest Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-asc.csv" TDigest Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-asc.csv" TDigest Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-asc.csv" TDigest Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-asc.csv" TDigest Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-des.csv" TDigest Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-des.csv" TDigest Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-des.csv" TDigest Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-des.csv" TDigest Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-des.csv" TDigest Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-des.csv" TDigest Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-rand.csv" TDigest Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-rand.csv" TDigest Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-rand.csv" TDigest Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-rand.csv" TDigest Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-rand.csv" TDigest Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-rand.csv" TDigest Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager " "
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager PVA
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-asc.csv" PVA Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-asc.csv" PVA Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-asc.csv" PVA Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-asc.csv" PVA Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-asc.csv" PVA Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-asc.csv" PVA Ascending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-des.csv" PVA Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-des.csv" PVA Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-des.csv" PVA Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-des.csv" PVA Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-des.csv" PVA Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-des.csv" PVA Descending
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager R "Data/Sets/1M/random-rand.csv" PVA Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager L "Data/Sets/1M/linear-rand.csv" PVA Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager N "Data/Sets/1M/normal-rand.csv" PVA Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager S "Data/Sets/1M/steps-rand.csv" PVA Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager P "Data/Sets/1M/poissongamma-rand.csv" PVA Random
java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager I "Data/Sets/1M/ipoissongamma-rand.csv" PVA Random