/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentile;

import java.util.ArrayList;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class PVA {

    private final ArrayList<GESet> sets;
    private final ArrayList<Double> desiredPercentiles;

    public PVA(double maxAllowedError, ArrayList<Double> theDesiredPercentiles) {
        sets = new ArrayList<>();
        desiredPercentiles = theDesiredPercentiles;
        for (double p : desiredPercentiles) {
            sets.add(new GESet(maxAllowedError, p));
        }
    }

    public Double getPercentileValue(double p) {
        for (int i = sets.size() - 1; i >= 0; --i) {
            if (desiredPercentiles.get(i) == p) {
                return sets.get(i).getPercentileValue();
            }
        }
        throw new RuntimeException("Percentile not found "+p);
    }
    
    public void addEntry(double entry){
        for (GESet set:sets){
            set.addEntry(entry);
        }
    }

}
