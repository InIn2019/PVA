/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentile;

import java.util.ArrayList;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class GESet {

    private final double desiredPercentile;
    private final double maxCapacityQuotient;
    private final ArrayList<GroupEntry> GESet;
    private final int rebalanceFactor;
    /**
     * Cachec\ Maximum Allowed Capacity per Complex Entry
     */
    private final ArrayList<Integer> maxCapacity;

    /**
     * In a GE-Set the GE-Set Size amount of data points that the GE-Set
     * represents.
     */
    private int GESetSize;

    public GESet(double maxAllowedError, Double percentileDesired) {
        GESet = new ArrayList<>();
        maxCapacityQuotient = maxAllowedError + 1.0;
        desiredPercentile = percentileDesired;
        maxCapacity = new ArrayList<>();
        rebalanceFactor = 3000;
    }

    public void addEntry(double entryValue) {
        if (GESetLength() > rebalanceFactor) {
            shrink();
        }
        ++GESetSize;
        GroupEntry entry = new GroupEntry(entryValue);
        //Base case empty list
        if (GESet.isEmpty()) {
            GESet.add(entry);
        } else {
            binarySearchAdd(entryValue, entry, 0, GESetLength() - 1);
        }

    }

    public int GESetLength() {
        return GESet.size();
    }

    /**
     * Uses binary search
     *
     * @param entryValue
     * @param entry
     * @param init
     * @param end
     */
    private void binarySearchAdd(double entryValue, GroupEntry entry, int init, int end) {
        GroupEntry container = GESet.get(init);
        if (container.getValue() > entryValue) {
            GESet.add(init, entry);
            return;
        }
        container = GESet.get(end);
        if (container.getValue() < entryValue) {
            GESet.add(end + 1, entry);
            return;
        }
        if (init + 1 == end) {
            GESet.add(end, entry);
            return;
        }
        int middle = (init + end) / 2;
        container = GESet.get(middle);
        if (container.getValue() == entryValue) {
            container.add(entryValue);
        } else if (container.getValue() > entryValue) {
            binarySearchAdd(entryValue, entry, init + 1, middle);
        } else {
            binarySearchAdd(entryValue, entry, middle, end - 1);
        }

    }

    public GroupEntry getGE(int index) {
        return GESet.get(index);
    }

    private int getMaxCapacity(int offset) {
        while (maxCapacity.size() <= offset) {
            int mx = (int) Math.pow(maxCapacityQuotient, maxCapacity.size());
            maxCapacity.add(mx);
        }
        return maxCapacity.get(offset);
    }

    private void shrink() {
        rebalanceLower(getGEIndex().first);
        rebalanceHigher(getGEIndex().first);

    }

    private void rebalanceLower(int offset) {
        GroupEntry rightGE = GESet.get(offset);
        int count = 0;
        while (offset > 1) {
            int leftOffset = offset - 1;
            GroupEntry leftGE = GESet.get(leftOffset);
            int offsetCapacity = getMaxCapacity(count);
            if ((rightGE.count + leftGE.count) < offsetCapacity) {
                GESet.remove(leftOffset);
                rightGE.add(leftGE);
            } else {
                ++count;
                rightGE = leftGE;
            }
            --offset;

        }
    }

    private void rebalanceHigher(int offset) {
        GroupEntry leftGE = GESet.get(offset);
        int count = 0;
        while (offset + 1 < GESetLength()) {
            int rightOffset = offset + 1;
            GroupEntry rightGE = GESet.get(rightOffset);
            int rightOffsetCapacity = getMaxCapacity(count);
            if ((leftGE.count + rightGE.count) < rightOffsetCapacity) {
                GESet.remove(rightOffset);
                leftGE.add(rightGE);
            } else {
                ++count;
                ++offset;
                leftGE = rightGE;
            }

        }
    }

    /**
     * Gets getGEIndex and the value of the element in complex entry
     *
     * @param percentileDesired
     * @return
     */
    private Pair<Integer, Integer> getGEIndex() {
        int desiredPosition = (int) (Math.round(GESetSize * desiredPercentile));
        int count = 0;
        int counter = 0;
        for (GroupEntry entry : GESet) {
            count += entry.count;
            if (desiredPosition <= count) {
                return new Pair<>(counter, count - desiredPosition);
            }
            ++counter;
        }
        return null;
    }

    public Double getPercentileValue() {
        Pair<Integer, Integer> index = getGEIndex();
        int GEIndex=index.first;
        int intraGEIndex=index.second;
        GroupEntry entry = GESet.get(GEIndex);
        if (GEIndex==0 || GEIndex==GESetLength()-1){
            return entry.getValue();
        }
        int offset = entry.count-intraGEIndex;
        int half = entry.count/2;
        if ( entry.count%2 ==1 && offset==half+1){
            return entry.getValue();
        }
        if (offset<half){
            GroupEntry left = GESet.get(GEIndex-1);
            double steps = half+(left.count/2);
            double difference = entry.getValue()-left.getValue();
            double step = half-offset;
            double  differencePercentage = step/steps;
            return entry.getValue()-(difference*differencePercentage);
        }else {
            
            GroupEntry right = GESet.get(GEIndex+1);
            double steps = half+(right.count/2);
            double difference = right.getValue()-entry.getValue();
            double step =  offset-half;
            double  differencePercentage = step/steps;
            return entry.getValue()+(difference*differencePercentage);
        }
    }

}
