/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentile;

import java.io.Serializable;

/**
 *
 * @author appvance
 * @param <T>
 * @param <U>
 */
public class Pair<T, U> implements Serializable {

    public T first;
    public U second;

    public Pair(T t, U u) {
        this.first = t;
        this.second = u;
    }

}
