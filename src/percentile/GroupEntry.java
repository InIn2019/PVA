/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentile;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class GroupEntry {
    public int count;
    private double totalValue;
    
    public double getValue(){
        return totalValue/count;
    }
    
    public void add(double another){
        totalValue+=another;
        ++count;
    }
    
    public void add(GroupEntry another){
        totalValue+=another.totalValue;
        count+=another.count;
    }
    
    public GroupEntry(double value){
        count=1;
        totalValue=value;
    }
}
