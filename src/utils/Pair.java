/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Luis Carlos Lara Lopez
 * @param <T>
 * @param <U>
 */
public class Pair<T, U> {

    public T first;
    public U second;

    public Pair(T t, U u) {
        this.first = t;
        this.second = u;
    }

}
