/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import datasorter.Asceding;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class Utils {

    /**
     * Rounds a double list to create a integer list
     *
     * @param list
     * @return
     */
    public static ArrayList<Integer> rounder(ArrayList<Double> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for (Double entry : list) {
            newList.add((int) (double) entry);
        }
        return newList;
    }

    /**
     * Clones a double list
     *
     * @param list
     * @return
     */
    public static ArrayList<Double> clone(ArrayList<Double> list) {
        ArrayList<Double> newList = new ArrayList<>();
        for (Double entry : list) {
            newList.add(entry);
        }
        return newList;
    }

    /**
     * Saves a double list to the defined file could be double or integer
     *
     * @param list
     * @param file
     * @throws Exception
     */
    public static void save(ArrayList<Double> list, String file) throws Exception {
        FileOutputStream fos = new FileOutputStream(file);
        for (Double entry : list) {
            fos.write((entry + "\n").getBytes());
        }
        fos.close();

    }

    /**
     * Saves a double list to the defined file could be double or integer
     *
     * @param list
     * @param file
     * @throws Exception
     */
    public static void saveRound(ArrayList<Double> list, String file) throws Exception {
        ArrayList<Integer> entries = rounder(list);
        Asceding a = new Asceding();
        a.sort(entries);
        FileOutputStream fos = new FileOutputStream(file);
        int current = -1;
        int count = 0;
        for (int entry : entries) {
            if (current != entry) {
                if (current != -1) {
                    String line = current + "," + count + "\n";
                    fos.write(line.getBytes());
                }
                current = entry;
                count = 1;
            } else {
                ++count;
            }
        }
        String line = current + "," + count + "\n";
        fos.write(line.getBytes());
        fos.close();

    }

    /**
     * Reads list from file
     *
     * @param file
     * @return
     * @throws Exception
     */
    public static ArrayList<Double> load(String file) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        ArrayList<Double> newList = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            newList.add(Double.parseDouble(line));
        }
        reader.close();
        return newList;

    }

    /**
     * Does a binary search over a sorted set
     *
     * @param sortedSet
     * @param x
     * @param minIndex
     * @param maxIndex
     * @return
     */
    public static int binarySearch(double[] sortedSet, double x, int minIndex, int maxIndex) {
        if (x < sortedSet[minIndex + 1]) {
            return minIndex;
        }
        if (x > sortedSet[maxIndex - 1]) {
            return maxIndex - 1;
        }
        int medIndex = (minIndex + maxIndex) / 2;
        if (x < sortedSet[medIndex]) {
            return binarySearch(sortedSet, x, minIndex + 1, medIndex);
        } else {
            return binarySearch(sortedSet, x, medIndex, maxIndex - 1);
        }
    }

    public static double factorial(double i) {
        if (i <= 1) {
            return 1;
        }
        return i * factorial(i - 1);
    }

    public static long getUsedMemoryKBytes() {
        Runtime instance = Runtime.getRuntime();
        return (instance.totalMemory() - instance.freeMemory()) / 1024;
    }

    /**
     * Calculates the size of a population based on the desired error, usually
     * around 5%
     *
     * @param N
     * @param e
     * @return
     */
    public static int yamaneSize(double N, double e) {
        return (int) (N / (1 + N * e * e));
    }

}
