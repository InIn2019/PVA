/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import datageneration.DataGenerator;
import datageneration.RandomGenerator;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class BrassardElementFinder {

    private static void sort5(ArrayList<Double> list, int elementIndex, int count) {
        ArrayList<Double> temp = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            temp.add(list.get(elementIndex + i));
        }
        Collections.sort(temp);
        for (int i = 0; i < count; ++i) {
            list.set(elementIndex + i, temp.get(i));
        }
    }

    /**
     * Sort in groups of 5
     *
     * @param list
     * @param elementIndex
     */
    private static void sort5(ArrayList<Double> list) {
        int size = list.size();
        for (int i = 0; i < size; ++i) {
            int maxCount = Math.min(5, size - i);
            sort5(list, i, maxCount);
        }

    }

    /**
     * Finds the value of a element as if they were ordered
     *
     * @param list
     * @param k
     * @return
     */
    public static double getValue(ArrayList<Double> list, int k) {
        if (list.size() > 50) {
            Collections.sort(list);
            return list.get(k);
        }
        sort5(list);
        ArrayList<Double> temp = new ArrayList<>();
        int size = list.size();
        for (int i = 2; i < size; i += 5) {
            temp.add(list.get(i));
        }
        double m = getValue(temp, (int) Math.ceil(temp.size() / 2.0));
        ArrayList<Double> s1 = new ArrayList<>();
        ArrayList<Double> s2 = new ArrayList<>();
        ArrayList<Double> s3 = new ArrayList<>();
        for (int i = 0; i < size; ++i) {
            double entry = list.get(i);
            if (entry < m) {
                s1.add(entry);
            } else if (entry == m) {
                s2.add(entry);
            } else {
                s3.add(entry);
            }

        }
        if (s1.size() >= k) {
            return getValue(s1, k);
        }
        if (s1.size() + s2.size() >= k) {
            return m;
        }
        return getValue(s3, k - (s1.size() + s2.size()));

    }

    public static void main(String[] args) throws Exception {
        ArrayList<Double> entriesA = new ArrayList<>();
        ArrayList<Double> entriesB = new ArrayList<>();
        RandomGenerator rg = new RandomGenerator(10, 1000);
        for (int i = DataGenerator.MaxEntries; i >= 0; --i) {
            double entry = rg.nextEntry();
            entriesA.add(entry);
            entriesB.add(entry);
        }
        Collections.sort(entriesA);
        for (int i = 0; i < 100; ++i) {
            int index = (int) (Math.random() * DataGenerator.MaxEntries);
            System.out.println("Index");
            System.out.println("Standard sort " + entriesA.get(index));
            System.out.println("Brassard sort " + getValue(entriesB, index));
        }

    }

}
