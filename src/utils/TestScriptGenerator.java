/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class TestScriptGenerator {

    private final String[] engines;
    private final String[] distributions;
    private final ArrayList<Pair<String, String>> orders;

    public TestScriptGenerator() {
        engines = new String[]{"Brassard", "Sampling", "TDigest", "PVA"};
        distributions = new String[]{"random", "linear", "normal", "steps","poissongamma", "ipoissongamma"};
        orders = new ArrayList<>();
        orders.add(new Pair<>("Ascending", "asc"));
        orders.add(new Pair<>("Descending", "des"));
        orders.add(new Pair<>("Random", "rand"));
    }

    public void createScripts() {
        System.out.println("del res.csv\ndel result.txt\n");
        for (String engine : engines) {
            String line = "java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager \" \"";
                  System.out.println(line);
              line = "java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager "+engine;
                  System.out.println(line);
            for (Pair<String, String> order : orders) {
                  line = "java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager "+order.first;
                  System.out.println(line);
                for (String distribution : distributions) {
                     line = "java -cp dist/Percentile.jar percentilefinder.PercentileFinderManager "
                            + distribution.substring(0, 1).toUpperCase() + " \"Data/Sets/10M/"
                            + distribution + "-" + order.second + ".csv\" " + engine + " " + order.first ;
                    System.out.println(line);
                }
            }
        }

    }

    public static void main(String[] arsg) {
        new TestScriptGenerator().createScripts();
    }
}
