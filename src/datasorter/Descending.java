/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasorter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class Descending implements Sorter {

    @Override
    public void sort(List<Double> entries) {
        Collections.sort(entries, new Comparator<Double>() {

            public int compare(Double a, Double b) {
                return (int) (a - b);
            }

        });
    }
}
