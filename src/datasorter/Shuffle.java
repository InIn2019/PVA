/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasorter;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class Shuffle implements Sorter {

    @Override

    public void sort(List<Double> entries) {
        Collections.shuffle(entries);
    }

}
