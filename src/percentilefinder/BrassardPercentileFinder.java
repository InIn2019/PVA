/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentilefinder;

import java.util.ArrayList;
import utils.BrassardElementFinder;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class BrassardPercentileFinder implements PercentileFinder {

    ArrayList<Double> entries;

    public BrassardPercentileFinder() {
        entries = new ArrayList<>();
    }

    @Override
    public void addEntry(double entry) {
        entries.add(entry);
    }

    @Override
    public double getPercentileValue(double percentile) {
        return BrassardElementFinder.getValue(entries, (int) (percentile * entries.size()));
    }
    
    
    public static void main(String[] args) throws Exception {
        PercentileFinderManager.process("R","Data/Sets/10M/random-des.csv", new BrassardPercentileFinder(), "Ascending");
    }
}
