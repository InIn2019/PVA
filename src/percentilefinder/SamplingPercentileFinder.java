/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentilefinder;

import java.util.ArrayList;
import utils.BrassardElementFinder;
import utils.Utils;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class SamplingPercentileFinder implements PercentileFinder {

    ArrayList<Double> entries;

    public SamplingPercentileFinder() {
        entries = new ArrayList<>();
    }

    @Override
    public void addEntry(double entry) {
        entries.add(entry);
    }

    @Override
    public double getPercentileValue(double percentile) {
        int size = entries.size();
        double sizeDouble = size;
        int yamaneSize = Utils.yamaneSize(size, 0.05);
        ArrayList<Double> sample = new ArrayList<>();
        while (--yamaneSize >= 0){
            sample.add(entries.get((int)(sizeDouble*Math.random())));
        }
        
        return BrassardElementFinder.getValue(sample, (int) (percentile * sample.size()));
    }
    
    
    public static void main(String[] args) throws Exception {
        PercentileFinderManager.process("R","Data/Sets/1M/random-des.csv", new BrassardPercentileFinder(), "Ascending");
    }
}
