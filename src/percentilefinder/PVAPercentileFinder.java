/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentilefinder;

import java.util.ArrayList;
import percentile.PVA;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class PVAPercentileFinder implements PercentileFinder {

    private final PVA pva;

    public PVAPercentileFinder() {
        ArrayList<Double> percentiles = new ArrayList<>();
        percentiles.add(0.50);
        percentiles.add(0.95);
        percentiles.add(0.99);
        pva = new PVA(0.05, percentiles);
    }


    @Override
    public void addEntry(double entry) {
        pva.addEntry(entry);
    }

    @Override
    public double getPercentileValue(double percentile) {
        return pva.getPercentileValue(percentile);
    }

    public static void main(String[] args) throws Exception {
        PercentileFinderManager.process("P","Data/Sets/1M/poissongamma-des.csv", new PVAPercentileFinder(), "Descending");
    }
}
