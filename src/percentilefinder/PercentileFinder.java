/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentilefinder;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public interface PercentileFinder {
    
    
    public void addEntry(double entry);
    
    
    public double getPercentileValue(double percentil);
}
