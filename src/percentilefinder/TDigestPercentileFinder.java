/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentilefinder;

import com.tdunning.math.stats.TDigest;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class TDigestPercentileFinder implements PercentileFinder {

    TDigest tDigest;


    public TDigestPercentileFinder() {
        tDigest = TDigest.createAvlTreeDigest(100);
    }

    @Override
    public void addEntry(double entry) {
        tDigest.add(entry);
    }

    @Override
    public double getPercentileValue(double percentile) {
        return tDigest.quantile(percentile);
    }
    
    
    public static void main(String[] args) throws Exception {
        PercentileFinderManager.process("R", "Data/Sets/10M/random-des.csv", new TDigestPercentileFinder(), "Ascending");
    }
}
