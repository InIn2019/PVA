/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentilefinder;

import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import utils.Utils;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class PercentileFinderManager {

    private final static byte[] anchor = " & ".getBytes();

    private final static NumberFormat twoDigits = new DecimalFormat("0.00");

    public static void main(String[] args) throws Exception {
        if (args.length == 1) {
            try (FileOutputStream fos = new FileOutputStream("results.txt", true)) {
                fos.write("\\hline\n".getBytes());
                fos.write(args[0].getBytes());
                fos.write("  &  &  & & \\\\\n".getBytes());
            }
            try (FileOutputStream fos = new FileOutputStream("res.csv", true)) {

                fos.write('\n');
            }
        } else {
            process(args[0], args[1], (PercentileFinder) Class.forName("percentilefinder." + args[2] + "PercentileFinder").newInstance(), args[3]);
        }
    }

    public static void process(String datasetId, String dataset, PercentileFinder pf, String sorttype) throws Exception {
        ArrayList<Double> list = Utils.load(dataset);
        System.gc();
        Thread.sleep(1000);
        System.gc();
        Thread.sleep(1000);
        long previousTime = System.currentTimeMillis();
        long previousKBytes = Utils.getUsedMemoryKBytes();
        for (Double entry : list) {
            pf.addEntry(entry);
        }
        System.gc();
        Thread.sleep(1000);
        double p50 = pf.getPercentileValue(0.50);
        double p95 = pf.getPercentileValue(0.95);
        double p99 = pf.getPercentileValue(0.99);
        long afterKBytes = Utils.getUsedMemoryKBytes();
        list.clear();
        long afterTime = System.currentTimeMillis();

        System.out.println("Dataset " + dataset);
        System.out.println("Sort " + sorttype);

        long durationMs = afterTime - previousTime;
        System.out.println("Duration msec " + durationMs);
        long SizeKB = (afterKBytes - previousKBytes);
        System.out.println("Size KBytes " + SizeKB);

        System.out.println("Percentile 50 " + p50);
        System.out.println("Percentile 95 " + p95);
        System.out.println("Percentile 99 " + p99);

        try (FileOutputStream fos = new FileOutputStream("results.txt", true)) {
            fos.write(datasetId.getBytes());
            fos.write(anchor);
            fos.write((SizeKB + "").getBytes());
            fos.write(anchor);
            fos.write((durationMs + "").getBytes());
            fos.write(anchor);
            fos.write(twoDigits.format(p50).getBytes());
            fos.write(anchor);
            fos.write(twoDigits.format(p95).getBytes());
            fos.write(anchor);
            fos.write(twoDigits.format(p99).getBytes());
            fos.write("\\\\\n".getBytes());
        }

        try (FileOutputStream fos = new FileOutputStream("res.csv", true)) {
            fos.write((SizeKB + "").getBytes());
            fos.write(',');
            fos.write((durationMs + "").getBytes());
            fos.write(',');
            fos.write(twoDigits.format(p50).getBytes());
            fos.write(',');
            fos.write(twoDigits.format(p95).getBytes());
            fos.write(',');
            fos.write(twoDigits.format(p99).getBytes());
            fos.write('\n');
        }
    }
}
