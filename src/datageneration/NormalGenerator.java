/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

import utils.Utils;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class NormalGenerator implements DataGenerator {

    private final static int size = 1000;

    private final double[] cumulative;
    private final double[] x;

    public NormalGenerator(double u, double o) {
        cumulative = new double[size];
        x = new double[size];
        double o2 = o * o;
        double two_o2 = 2 * o2;
        double sql_two_pi_o2 = Math.sqrt(1.0 / (Math.PI * two_o2));

        double offset = (15 * o) / (double)size;//multiplied by 10 divided by 1000
        double currentX = u - ((15 * o)/2);//Init point
        double currentCumulative = 0;
        for (int i = 0; i < size; ++i) {
            double currentU = currentX - u;
            double exp = -(currentU * currentU) / two_o2;
            double value = sql_two_pi_o2 * Math.pow(Math.E, exp);
            currentX += offset;
            currentCumulative += value * offset;

            cumulative[i] = currentCumulative;
            x[i] = currentX;
            
            System.out.println(currentX+" "+ value+" "+currentCumulative);
        }

    }


    @Override
    public double nextEntry() {
        double value = Math.random();
        int index = Utils.binarySearch(cumulative,value, 0, size);
        double cumulativeDistance = cumulative[index + 1] / cumulative[index];
        double elementDistance = value - cumulative[index];
        double distancePercentage = elementDistance / cumulativeDistance;

        double xDistance = x[index + 1] / x[index];
        return x[index] + distancePercentage * xDistance;

    }

    public static void main(String[] args) throws Exception {
        NormalGenerator rg = new NormalGenerator(500, 100);
        DataGenerator.createRoundData(rg, "normal");
    }
}
