/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

import utils.Utils;
import  org.apache.commons.math3.special.Gamma;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class PoissonGammaGenerator implements DataGenerator {

    private final static int lampda = 7;
    private final static int K = 25;
    private final static int stepCount = 100;
    private final static double step = (double)K/(double)stepCount;
    private final static double[] cumulative;

    static {
        cumulative = new double[stepCount];
        double currentCumulative = 0;
        double offset=0;
        for (int i = 0; i < stepCount; ++i) {
            double P = (Math.pow(lampda, offset) * Math.pow(Math.E, -lampda)) / Gamma.gamma(offset+1);
            currentCumulative += P*step;
            offset+=step;
            cumulative[i] = currentCumulative;
        }
    }

    private final double begin;
    private final double length;

    public PoissonGammaGenerator(double theBegin, double end) {
        begin = theBegin;
        length = end - begin;
    }

    @Override
    public double nextEntry() {
        //Discrete part
        double y = Math.random();
        int index = Utils.binarySearch(cumulative, y, 0, stepCount - 1);
       
     
        //Continous part
        //y=mx+b
        double x1 = index;
        double y1=cumulative[index];
        double y2=cumulative[index+1];
        y = y1+((y2-y1)*Math.random());
        double m = y2-y1/1;
        double b = y1-m*x1;
        double x = (y-b)/m;
        
        double entry = (x/(double)stepCount)*length+begin;
       return entry;
    }
    

    public static void main(String[] args) throws Exception {
        PoissonGammaGenerator rg = new PoissonGammaGenerator(10, 1000);

        for (int i = 0; i < stepCount; ++i) {
            System.out.println(i + " " + cumulative[i]);
        }
        DataGenerator.createRoundData(rg, "possiongamma");
    }
}
