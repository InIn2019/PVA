/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import utils.Utils;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public interface DataGenerator {

    public static int MaxEntries = 1000000;

    public double nextEntry();

    public static void createRoundData(DataGenerator generator, String name) throws Exception {
        Utils.saveRound(createData(generator), name + "-round.csv");

    }

    /**
     * Generates data in a folder
     *
     * @param generator
     * @param entriesCount
     * @param folder
     * @param name
     * @throws Exception
     */
    public static void createData(DataGenerator generator, int entriesCount, String folder, String name) throws Exception {
        new File(folder).mkdirs();
        ArrayList<Double> entries = new ArrayList<>();
        for (int i = entriesCount; i >= 0; --i) {
            entries.add(generator.nextEntry());
        }
        Collections.sort(entries);
        Utils.save(entries, folder + "/" + name + "-asc.csv");
        Collections.sort(entries, Collections.reverseOrder());
        Utils.save(entries, folder + "/" + name + "-des.csv");
        Collections.shuffle(entries);
        Utils.save(entries, folder + "/" + name + "-rand.csv");

    }

    public static ArrayList<Double> createData(DataGenerator generator) throws Exception {
        ArrayList<Double> entries = new ArrayList<>();
        for (int i = DataGenerator.MaxEntries; i >= 0; --i) {
            entries.add(generator.nextEntry());
        }
        return entries;
    }

    public static void createData(DataGenerator generator, String name) throws Exception {
        Utils.saveRound(createData(generator), "data-" + name + ".csv");
    }

}
