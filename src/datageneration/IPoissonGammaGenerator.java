/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class IPoissonGammaGenerator extends PoissonGammaGenerator {

    private final double end;

    public IPoissonGammaGenerator(double theBegin, double theEnd) {
        super(theBegin,theEnd);
        end = theEnd;
    }

    @Override
    public double nextEntry() {
       return end-super.nextEntry();
    }
    

    public static void main(String[] args) throws Exception {
        IPoissonGammaGenerator rg = new IPoissonGammaGenerator(10, 1000);
        DataGenerator.createRoundData(rg, "ipossiongamma");
    }
}
