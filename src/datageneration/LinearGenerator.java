/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class LinearGenerator implements DataGenerator {

    double current;
    double offset;

    public LinearGenerator(double theMin, double theMax, int total) {
        current = theMin;
        offset = (theMax - theMin) / (double) total;
    }

    @Override
    public double nextEntry() {
        double temp = current;
        current += offset;
        return temp;
    }

    public static void main(String[] args) throws Exception {
        LinearGenerator linear = new LinearGenerator(10, 1000, DataGenerator.MaxEntries);
        DataGenerator.createRoundData(linear, "linear");
    }
}
