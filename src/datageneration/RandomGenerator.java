/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class RandomGenerator implements DataGenerator {
    
    double base;
    double size;
    
    public RandomGenerator(double theMin, double theMax) {
        base = theMin;
        size = theMax - theMin;
    }
    
    @Override
    public double nextEntry() {
        return base + Math.random() * size;
    }
    
    public static void main(String[] args) throws Exception {
        RandomGenerator rg = new RandomGenerator(10, 1000);
        DataGenerator.createRoundData(rg,"random");
    }
}
