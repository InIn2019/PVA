/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class GenerateSets {

    private static void createDataSet(int maxEntries, String folder) throws Exception {

        RandomGenerator rg = new RandomGenerator(10, 1000);
        DataGenerator.createData(rg, maxEntries, folder, "random");

        RandomGenerator linear = new RandomGenerator(10, 1000);
        DataGenerator.createData(linear, maxEntries, folder, "linear");
        
        RandomGenerator normal = new RandomGenerator(10, 1000);
        DataGenerator.createData(normal, maxEntries, folder, "normal");

        PoissonGammaGenerator pg = new PoissonGammaGenerator(10, 1000);
        DataGenerator.createData(pg, maxEntries, folder, "poissongamma");
        
        IPoissonGammaGenerator ipg = new IPoissonGammaGenerator(10, 1000);
        DataGenerator.createData(ipg, maxEntries, folder, "ipoissongamma");
        
        PoissonLinearGenerator steps = new PoissonLinearGenerator(10, 1000);
        DataGenerator.createData(steps, maxEntries, folder, "steps");

    }

    public static void main(String[] args) throws Exception {
        int maxEntries = 10*1000;//1Millios
        createDataSet(maxEntries, "Data/Sets/1M");
    }
}
