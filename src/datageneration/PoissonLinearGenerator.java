/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datageneration;

import utils.Utils;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class PoissonLinearGenerator implements DataGenerator {

    private final static int lampda = 7;
    private final static int K = 25;
    private final static double[] cumulative;

    static {
        cumulative = new double[K];
        double currentCumulative = 0;
        for (int i = 0; i < K; ++i) {
            double P = (Math.pow(lampda, i) * Math.pow(Math.E, -lampda)) / Utils.factorial(i);
            currentCumulative += P;
            cumulative[i] = currentCumulative;
        }
    }

    private final double begin;
    private final double length;

    public PoissonLinearGenerator(double theBegin, double end) {
        begin = theBegin;
        length = end - begin;
    }

    @Override
    public double nextEntry() {
        //Discrete part
        double y = Math.random();
        int index = Utils.binarySearch(cumulative, y, 0, K - 1);
       
     
        //Continous part
        //y=mx+b
        double x1 = index;
        double y1=cumulative[index];
        double y2=cumulative[index+1];
        y = y1+((y2-y1)*Math.random());
        double m = y2-y1/1;
        double b = y1-m*x1;
        double x = (y-b)/m;
        
        double entry = (x/(double)K)*length+begin;
       return entry;
    }
    

    public static void main(String[] args) throws Exception {
        PoissonLinearGenerator rg = new PoissonLinearGenerator(10, 1000);

        for (int i = 0; i < K; ++i) {
            System.out.println(i + " " + cumulative[i]);
        }
        DataGenerator.createRoundData(rg, "steps");
    }
}
