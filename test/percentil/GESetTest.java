/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentil;

import java.util.ArrayList;
import org.junit.Test;
import percentile.GESet;
import percentile.GroupEntry;

/**
 *
 * @author appvance
 */
public class GESetTest {
    
    @Test
    public void doTest() {
        int count =10000000;
        GESet a = new GESet(0.05,0.955);
        for (int i = count; i > 0; --i) {
            a.addEntry(Math.random());
        }
        int length=a.GESetLength();
        System.out.print("TOTQAL "+length);
        
        int secondCount =0;
        for (int i = 0;i<length;++i){
            GroupEntry ge=a.getGE(i);
            //System.out.println(ge.getValue()+" "+ge.count);
            secondCount+=ge.count;
        }
        System.out.println("Second Count "+secondCount);
        
        System.out.println("Percentile "+a.getPercentileValue());
    }
}
