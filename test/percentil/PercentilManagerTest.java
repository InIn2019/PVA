/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percentil;

import percentile.PVA;
import com.tdunning.math.stats.TDigest;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Luis Carlos Lara Lopez
 */
public class PercentilManagerTest {

    @Test
    public void doTest() {
        ArrayList<Double> percentiles = new ArrayList<>();
        percentiles.add(0.956);

        int k = 10000;

        PVA fpm = new PVA(0.01, percentiles);
        TDigest tDigest = TDigest.createAvlTreeDigest(100);
        for (int i = 0; i < k; ++i) {
            fpm.addEntry(k - i);
            tDigest.add(k - 1);
        }
        System.out.println("Percentil " + fpm.getPercentileValue(0.956));

        System.out.println();
        System.out.println("tDigest" + tDigest.quantile(0.956));
        System.out.println("Centroids " + tDigest.centroidCount());
        PVA spm = new PVA(0.01, percentiles);

        TDigest stDigest = TDigest.createAvlTreeDigest(100);
        for (int i = 1; i <= k; ++i) {
            spm.addEntry(i);
            stDigest.add(i);
        }

        System.out.println("Percentil " + fpm.getPercentileValue(0.956));
        System.out.println();
        System.out.println("stDigest" + stDigest.quantile(0.956));
        System.out.println("Centroids " + stDigest.centroidCount());

        PVA tpm = new PVA(0.01, percentiles);
        TDigest ttDigest = TDigest.createAvlTreeDigest(100);
        for (int i = 1; i <= k; ++i) {
            double random = (Math.random() * k);
            tpm.addEntry(random);
            ttDigest.add(random);
        }
    }
}
